import ujson
import urequests

from configsystem import SYSTEM_ID, STOP_ON_EXCEPTION
from configbackend import POST_URL

from sntime import get_current_datetime_str
from snlogs import write_log


SERVER_WRITE_SUCCESS_CODE = 201

def post_measurement(current_timestamp, measurement, value, sensor_id, sensor_location, unit):
    try:
        datetime_str = get_current_datetime_str(timestamp=current_timestamp)  # Convert int to ISO string

        measurement_dict = {
            "system_id": SYSTEM_ID,
            "sensor_id": sensor_id,
            "sensor_location": sensor_location,
            "timestamp": datetime_str,    # e.g. "2021-01-01T00:00:00+0100",
            "measurement": measurement,
            "value": value,
            "unit": unit
        }

        ## For some strange reasons, this doesn't work ;
        ## urequests send the jsonified version of "measurement_dict" in "request.data"
        ## but "request.json" is empty...
        #resp = urequests.post(POST_URL, json=measurement_dict)

        data_str = ujson.dumps(measurement_dict)
        print(data_str)
        resp = urequests.post(POST_URL, data=data_str) #, headers=headers_dict)

        if resp.status_code != SERVER_WRITE_SUCCESS_CODE:
            msg_str = get_current_datetime_str() + ": " + data_str + "; "
            msg_str += "Error while sending measurement in post_measurement(); "
            msg_str += "Returned server status_code is " + str(resp.status_code) + "; "
            msg_str += "Returned server reason is " + resp.reason.decode('utf-8') + ";"
            msg_str += "The raw response is " + resp.text + "\n\n"
            write_log(msg_str, print_msg=True)

        resp.close()   # c.f. https://forum.micropython.org/viewtopic.php?t=7771
    except Exception as e:
        msg_str = get_current_datetime_str() + ": " + str(e) + "; An unknown error occurred while sending measurement in post_measurement()" + ";"
        write_log(msg_str, print_msg=True)
        if STOP_ON_EXCEPTION:
            raise