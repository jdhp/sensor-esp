"""
C.f. https://docs.micropython.org/en/latest/esp8266/tutorial/dht.html#temperature-and-humidity
"""

import dht
import machine

from sntime import get_current_timestamp, get_current_datetime_str

class SNDHT:

    def __init__(self, gpio_pin, sensor_id, sensor_location, sensor_version="DHT22"):
        self.gpio_pin = gpio_pin
        self.sensor_id = sensor_id
        self.sensor_location = sensor_location
        self.sensor_version = sensor_version

        if self.sensor_version in ("DHT22", "AM2302"):
            self.dht = dht.DHT22(machine.Pin(self.gpio_pin))
        elif self.sensor_version == "DHT11":
            self.dht = dht.DHT11(machine.Pin(self.gpio_pin))
        else:
            raise ValueError(f"Unknown DHT version {self.sensor_version}")


    def measure(self):
        current_timestamp = get_current_timestamp()
        #current_datetime_str = get_current_datetime_str()
        self.dht.measure()

        temperature = self.dht.temperature()
        humidity = self.dht.humidity()

        measure_dict_list = [
            {
                "current_timestamp": current_timestamp,
                "measurement": "temperature",
                "value": temperature,
                "sensor_name": self.sensor_version,
                "sensor_id": self.sensor_id,
                "sensor_location": self.sensor_location,
                "unit": "Celsius"
            },
            {
                "current_timestamp": current_timestamp,
                "measurement": "humidity",
                "value": humidity,
                "sensor_name": self.sensor_version,
                "sensor_id": self.sensor_id,
                "sensor_location": self.sensor_location,
                "unit": "%"
            }
        ]

        return measure_dict_list
