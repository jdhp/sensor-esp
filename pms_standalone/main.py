import pms5003
import machine
import uasyncio as asyncio

uart = machine.UART(2, tx=17, rx=16, baudrate=9600)

pm = pms5003.PMS5003(uart)

class PMSCallback():
    def __init__(self, num_measures_to_aggregate):
        self.num_measures_to_aggregate = num_measures_to_aggregate
        self.reset()

    def reset(self):
        self.measure_index = 0

        self.avg_pm010_std = 0         # Concentration Units (standard) PM 1.0
        self.avg_pm025_std = 0         # Concentration Units (standard) PM 2.5
        self.avg_pm100_std = 0         # Concentration Units (standard) PM 10
        self.avg_pm010_env = 0         # Concentration Units (environmental) PM 1.0
        self.avg_pm025_env = 0         # Concentration Units (environmental) PM 2.5
        self.avg_pm100_env = 0         # Concentration Units (environmental) PM 10
        self.avg_particles_003um = 0   # Particles > 0.3um / 0.1L air
        self.avg_particles_005um = 0   # Particles > 0.5um / 0.1L air
        self.avg_particles_010um = 0   # Particles > 1.0um / 0.1L air
        self.avg_particles_025um = 0   # Particles > 2.5um / 0.1L air
        self.avg_particles_050um = 0   # Particles > 5.0um / 0.1L air
        self.avg_particles_100um = 0   # Particles > 10 um / 0.1L air

        self.min_pm010_std = float("inf")
        self.min_pm025_std = float("inf")
        self.min_pm100_std = float("inf")
        self.min_pm010_env = float("inf")
        self.min_pm025_env = float("inf")
        self.min_pm100_env = float("inf")
        self.min_particles_003um = float("inf")
        self.min_particles_005um = float("inf")
        self.min_particles_010um = float("inf")
        self.min_particles_025um = float("inf")
        self.min_particles_050um = float("inf")
        self.min_particles_100um = float("inf")

        self.max_pm010_std = float("-inf")
        self.max_pm025_std = float("-inf")
        self.max_pm100_std = float("-inf")
        self.max_pm010_env = float("-inf")
        self.max_pm025_env = float("-inf")
        self.max_pm100_env = float("-inf")
        self.max_particles_003um = float("-inf")
        self.max_particles_005um = float("-inf")
        self.max_particles_010um = float("-inf")
        self.max_particles_025um = float("-inf")
        self.max_particles_050um = float("-inf")
        self.max_particles_100um = float("-inf")

    def __call__(self):
        if self.measure_index >= self.num_measures_to_aggregate:
            pm.print()

            print("Concentration Units (standard) PM 1.0      ", self.avg_pm010_std, self.min_pm010_std, self.max_pm010_std)
            print("Concentration Units (standard) PM 2.5      ", self.avg_pm025_std, self.min_pm025_std, self.max_pm025_std)
            print("Concentration Units (standard) PM 10       ", self.avg_pm100_std, self.min_pm100_std, self.max_pm100_std)
            print("Concentration Units (environmental) PM 1.0 ", self.avg_pm010_env, self.min_pm010_env, self.max_pm010_env)
            print("Concentration Units (environmental) PM 2.5 ", self.avg_pm025_env, self.min_pm025_env, self.max_pm025_env)
            print("Concentration Units (environmental) PM 10  ", self.avg_pm100_env, self.min_pm100_env, self.max_pm100_env)
            print("Particles > 0.3um / 0.1L air               ", self.avg_particles_003um, self.min_particles_003um, self.max_particles_003um)
            print("Particles > 0.5um / 0.1L air               ", self.avg_particles_005um, self.min_particles_005um, self.max_particles_005um)
            print("Particles > 1.0um / 0.1L air               ", self.avg_particles_010um, self.min_particles_010um, self.max_particles_010um)
            print("Particles > 2.5um / 0.1L air               ", self.avg_particles_025um, self.min_particles_025um, self.max_particles_025um)
            print("Particles > 5.0um / 0.1L air               ", self.avg_particles_050um, self.min_particles_050um, self.max_particles_050um)
            print("Particles > 10 um / 0.1L air               ", self.avg_particles_100um, self.min_particles_100um, self.max_particles_100um)

            self.reset()
        else:
            pm010_std = pm.pm10_standard
            pm025_std = pm.pm25_standard
            pm100_std = pm.pm100_standard
            pm010_env = pm.pm10_env
            pm025_env = pm.pm25_env
            pm100_env = pm.pm100_env
            particles_003um = pm.particles_03um
            particles_005um = pm.particles_05um
            particles_010um = pm.particles_10um
            particles_025um = pm.particles_25um
            particles_050um = pm.particles_50um
            particles_100um = pm.particles_100um

            self.avg_pm010_std += pm010_std  / self.num_measures_to_aggregate
            self.avg_pm025_std += pm025_std  / self.num_measures_to_aggregate
            self.avg_pm100_std += pm100_std  / self.num_measures_to_aggregate
            self.avg_pm010_env += pm010_env  / self.num_measures_to_aggregate 
            self.avg_pm025_env += pm025_env  / self.num_measures_to_aggregate 
            self.avg_pm100_env += pm100_env  / self.num_measures_to_aggregate
            self.avg_particles_003um += particles_003um / self.num_measures_to_aggregate
            self.avg_particles_005um += particles_005um / self.num_measures_to_aggregate
            self.avg_particles_010um += particles_010um / self.num_measures_to_aggregate
            self.avg_particles_025um += particles_025um / self.num_measures_to_aggregate
            self.avg_particles_050um += particles_050um / self.num_measures_to_aggregate
            self.avg_particles_100um += particles_100um / self.num_measures_to_aggregate

            self.min_pm010_std = min(self.min_pm010_std, pm010_std)
            self.min_pm025_std = min(self.min_pm025_std, pm025_std)
            self.min_pm100_std = min(self.min_pm100_std, pm100_std)
            self.min_pm010_env = min(self.min_pm010_env, pm010_env)
            self.min_pm025_env = min(self.min_pm025_env, pm025_env)
            self.min_pm100_env = min(self.min_pm100_env, pm100_env)
            self.min_particles_003um = min(self.min_particles_003um, particles_003um)
            self.min_particles_005um = min(self.min_particles_005um, particles_005um)
            self.min_particles_010um = min(self.min_particles_010um, particles_010um)
            self.min_particles_025um = min(self.min_particles_025um, particles_025um)
            self.min_particles_050um = min(self.min_particles_050um, particles_050um)
            self.min_particles_100um = min(self.min_particles_100um, particles_100um)

            self.max_pm010_std = max(self.max_pm010_std, pm010_std)
            self.max_pm025_std = max(self.max_pm025_std, pm025_std)
            self.max_pm100_std = max(self.max_pm100_std, pm100_std)
            self.max_pm010_env = max(self.max_pm010_env, pm010_env)
            self.max_pm025_env = max(self.max_pm025_env, pm025_env)
            self.max_pm100_env = max(self.max_pm100_env, pm100_env)
            self.max_particles_003um = max(self.max_particles_003um, particles_003um)
            self.max_particles_005um = max(self.max_particles_005um, particles_005um)
            self.max_particles_010um = max(self.max_particles_010um, particles_010um)
            self.max_particles_025um = max(self.max_particles_025um, particles_025um)
            self.max_particles_050um = max(self.max_particles_050um, particles_050um)
            self.max_particles_100um = max(self.max_particles_100um, particles_100um)

            self.measure_index += 1


callback_func = PMSCallback(60 * 5)
pm.registerCallback(callback_func)

loop = asyncio.get_event_loop()
loop.run_forever()