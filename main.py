"""
Push modules
============

$ ampy -p /dev/ttyUSB0 -b 115200 put 'main.py'


Interactive test with picocom
=============================

$ picocom --baud=115200 /dev/ttyUSB0

>>> import main
>>> dht_obj, ldr_adc_obj = main.init()
>>> dht_obj.measure()
>>> dht_obj.temperature()
21.8
>>> dht_obj.humidity()
42.8
"""

from configbackend import BACKEND_TYPE
from configsensors import *
from configsystem import SYSTEM_ID, SYSTEM_LOCATION, SLEEP_TIME_SEC, STOP_ON_EXCEPTION

import dht
import esp
import esp32
import machine
import ujson
import time

from snwifi import connect_to_wifi, disconnect_from_wifi
from sntime import synchronize_ntp, get_current_timestamp, get_current_datetime_str
from snlogs import write_log

if BACKEND_TYPE == "RESTFUL":
    from sninfluxdb import post_measurement
elif BACKEND_TYPE == "INFLUXDB2":
    from snrest import post_measurement
elif BACKEND_TYPE == "MQTT":
    from snmqtt import post_measurement
else:
    write_log(f"Unknown backend {BACKEND_TYPE}", print_msg=True)


if PMS5003_SENSOR_ID is not None:
    import pms5003
    import uasyncio as asyncio


# INIT ########################################################################

def init():

    # SENSOR OBJECTS ##########################################################

    if DHT22_SENSOR_CONFIG is not None:
        for dht_dict in DHT22_SENSOR_CONFIG:
            dht_obj = dht.DHT22(machine.Pin(dht_dict["pin"]))
            dht_dict["dht_obj"] = dht_obj

    if LDR_SENSOR_CONFIG is not None:
        for ldr_dict in LDR_SENSOR_CONFIG:
            ldr_dict["adc_obj"] = machine.ADC(machine.Pin(ldr_dict["pin"]))

    if HCSR501_PIR_SENSOR_CONFIG is not None:
        for pir_sensor_dict in HCSR501_PIR_SENSOR_CONFIG:
            pir_sensor_dict["pin"].irq(trigger=machine.Pin.IRQ_RISING, handler=hcsr501_pir_callback)  # TODO: Is it ok if pir_pin object is destroyed at the end for this function ???
            pir_sensor_dict["count"] = 0

    if PMS5003_SENSOR_ID is not None:
        uart = machine.UART(2, tx=17, rx=16, baudrate=9600)
        pms_obj = pms5003.PMS5003(uart)

        callback_func = PMSCallback(SLEEP_TIME_SEC, pms_obj)
        pms_obj.registerCallback(callback_func)


# SENSORS #####################################################################

def hcsr501_pir_callback(pin):
    for pir_sensor_dict in HCSR501_PIR_SENSOR_CONFIG:
        if pir_sensor_dict["pin"] == pin:
            pir_sensor_dict["count"] += 1
            break

    #print(pin, time.time(), pin.value())


def read_and_post_hcsr501_pir_sensors():
    if HCSR501_PIR_SENSOR_CONFIG is not None:
        for pir_sensor_dict in HCSR501_PIR_SENSOR_CONFIG:
            current_timestamp = get_current_timestamp()

            post_measurement(current_timestamp=current_timestamp,
                             measurement="PIR",
                             value=pir_sensor_dict["count"],
                             sensor_id=pir_sensor_dict["sensor_id"],
                             sensor_location=HCSR501_PIR_SENSOR_LOCATION,
                             unit="Triggers")
        
            pir_sensor_dict["count"] = 0


def read_and_post_dht22_sensors():
    if DHT22_SENSOR_CONFIG is not None:
        for dht_dict in DHT22_SENSOR_CONFIG:
            current_timestamp = get_current_timestamp()

            dht_dict["dht_obj"].measure()

            if BACKEND_TYPE == "MQTT":

                mqtt_payload_dict = {
                    "temperature": dht_dict["dht_obj"].temperature(),
                    "humidity": dht_dict["dht_obj"].humidity(),
                    "timestamp": current_timestamp,
                    # "units_dict": {
                    #     "temperature": "Celsius",
                    #     "humidity": "%"
                    # }
                }

                post_measurement(mqtt_payload_dict=mqtt_payload_dict,
                                 sensor_name="dht22_ambient_sensor",
                                 sensor_id=dht_dict["sensor_id"],
                                 sensor_location=DHT22_SENSOR_LOCATION)

            else:

                post_measurement(current_timestamp=current_timestamp,
                                measurement="temperature",
                                value=dht_dict["dht_obj"].temperature(),
                                sensor_id=dht_dict["sensor_id"],
                                sensor_location=DHT22_SENSOR_LOCATION,
                                unit="Celsius")

                post_measurement(current_timestamp=current_timestamp,
                                measurement="humidity",
                                value=dht_dict["dht_obj"].humidity(),
                                sensor_id=dht_dict["sensor_id"],
                                sensor_location=DHT22_SENSOR_LOCATION,
                                unit="%")


def read_and_post_ldr_sensor():
    if LDR_SENSOR_CONFIG is not None:
        for ldr_dict in LDR_SENSOR_CONFIG:
            current_timestamp = get_current_timestamp()

            ldr_raw_level = ldr_dict["adc_obj"].read()
            ldr_percent_level = float(ldr_raw_level) / 4095. * 100.

            post_measurement(current_timestamp=current_timestamp,
                            measurement="light",
                            value=ldr_percent_level,
                            sensor_id=ldr_dict["sensor_id"],
                            sensor_location=LDR_SENSOR_LOCATION,
                            unit="%")


def read_and_post_esp32_internal_sensors():
    current_timestamp = get_current_timestamp()

    # http://docs.micropython.org/en/latest/esp32/quickref.html#general-board-control
    esp_flash_size = esp.flash_size()
    esp32_hall_sensor = esp32.hall_sensor()                   # read the internal hall sensor
    esp32_mcu_temperature_farenheit = esp32.raw_temperature() # read the internal temperature of the MCU, in Farenheit
    esp32_mcu_temperature_celsius = (esp32_mcu_temperature_farenheit - 32.0) / 1.8

    if BACKEND_TYPE == "MQTT":

        mqtt_payload_dict = {
            "flash_size": esp_flash_size,
            "hall_sensor": esp32_hall_sensor,
            "esp32_internal_temperature": esp32_mcu_temperature_celsius,
            "timestamp": current_timestamp,
            # "units_dict": {
            #     "flash_size": "raw",
            #     "hall_sensor": "raw",
            #     "esp32_internal_temperature": "Celsius"
            # }
        }

        post_measurement(mqtt_payload_dict=mqtt_payload_dict,
                         sensor_name="esp32_internal_sensors",
                         sensor_id=SYSTEM_ID,
                         sensor_location=SYSTEM_LOCATION)

    else:

        post_measurement(current_timestamp=current_timestamp,
                         measurement="flash_size",
                         value=esp_flash_size,
                         sensor_id=SYSTEM_ID,
                         sensor_location=SYSTEM_LOCATION,
                         unit="raw")

        post_measurement(current_timestamp=current_timestamp,
                         measurement="hall_sensor",
                         value=esp32_hall_sensor,
                         sensor_id=SYSTEM_ID,
                         sensor_location=SYSTEM_LOCATION,
                         unit="raw")

        post_measurement(current_timestamp=current_timestamp,
                         measurement="esp32_internal_temperature",
                         value=esp32_mcu_temperature_celsius,
                         sensor_id=SYSTEM_ID,
                         sensor_location=SYSTEM_LOCATION,
                         unit="Celsius")


class PMSCallback():
    def __init__(self, num_measures_to_aggregate, pms_obj):
        self.num_measures_to_aggregate = num_measures_to_aggregate
        self.pms_obj = pms_obj

        self.reset()

    def reset(self):
        self.measure_index = 0

        self.avg_pm010_std = 0         # Concentration Units (standard) PM 1.0
        self.avg_pm025_std = 0         # Concentration Units (standard) PM 2.5
        self.avg_pm100_std = 0         # Concentration Units (standard) PM 10
        self.avg_pm010_env = 0         # Concentration Units (environmental) PM 1.0
        self.avg_pm025_env = 0         # Concentration Units (environmental) PM 2.5
        self.avg_pm100_env = 0         # Concentration Units (environmental) PM 10
        self.avg_particles_003um = 0   # Particles > 0.3um / 0.1L air
        self.avg_particles_005um = 0   # Particles > 0.5um / 0.1L air
        self.avg_particles_010um = 0   # Particles > 1.0um / 0.1L air
        self.avg_particles_025um = 0   # Particles > 2.5um / 0.1L air
        self.avg_particles_050um = 0   # Particles > 5.0um / 0.1L air
        self.avg_particles_100um = 0   # Particles > 10 um / 0.1L air

        self.min_pm010_std = float("inf")
        self.min_pm025_std = float("inf")
        self.min_pm100_std = float("inf")
        self.min_pm010_env = float("inf")
        self.min_pm025_env = float("inf")
        self.min_pm100_env = float("inf")
        self.min_particles_003um = float("inf")
        self.min_particles_005um = float("inf")
        self.min_particles_010um = float("inf")
        self.min_particles_025um = float("inf")
        self.min_particles_050um = float("inf")
        self.min_particles_100um = float("inf")

        self.max_pm010_std = float("-inf")
        self.max_pm025_std = float("-inf")
        self.max_pm100_std = float("-inf")
        self.max_pm010_env = float("-inf")
        self.max_pm025_env = float("-inf")
        self.max_pm100_env = float("-inf")
        self.max_particles_003um = float("-inf")
        self.max_particles_005um = float("-inf")
        self.max_particles_010um = float("-inf")
        self.max_particles_025um = float("-inf")
        self.max_particles_050um = float("-inf")
        self.max_particles_100um = float("-inf")

    def __call__(self):
        try:
            connect_to_wifi()
            synchronize_ntp()

            if self.measure_index >= self.num_measures_to_aggregate:
                #self.pms_obj.print()

                print()
                print("-" * 60)
                print(get_current_datetime_str())
                print("Concentration Units (standard) PM 1.0      ", self.avg_pm010_std, self.min_pm010_std, self.max_pm010_std)
                print("Concentration Units (standard) PM 2.5      ", self.avg_pm025_std, self.min_pm025_std, self.max_pm025_std)
                print("Concentration Units (standard) PM 10       ", self.avg_pm100_std, self.min_pm100_std, self.max_pm100_std)
                print("Concentration Units (environmental) PM 1.0 ", self.avg_pm010_env, self.min_pm010_env, self.max_pm010_env)
                print("Concentration Units (environmental) PM 2.5 ", self.avg_pm025_env, self.min_pm025_env, self.max_pm025_env)
                print("Concentration Units (environmental) PM 10  ", self.avg_pm100_env, self.min_pm100_env, self.max_pm100_env)
                print("Particles > 0.3um / 0.1L air               ", self.avg_particles_003um, self.min_particles_003um, self.max_particles_003um)
                print("Particles > 0.5um / 0.1L air               ", self.avg_particles_005um, self.min_particles_005um, self.max_particles_005um)
                print("Particles > 1.0um / 0.1L air               ", self.avg_particles_010um, self.min_particles_010um, self.max_particles_010um)
                print("Particles > 2.5um / 0.1L air               ", self.avg_particles_025um, self.min_particles_025um, self.max_particles_025um)
                print("Particles > 5.0um / 0.1L air               ", self.avg_particles_050um, self.min_particles_050um, self.max_particles_050um)
                print("Particles > 10 um / 0.1L air               ", self.avg_particles_100um, self.min_particles_100um, self.max_particles_100um)

                current_timestamp = get_current_timestamp()

                post_measurement(current_timestamp=current_timestamp,
                                 measurement="PM_1_std",
                                 value={
                                     "avg": self.avg_pm010_std,
                                     "min": self.min_pm010_std,
                                     "max": self.max_pm010_std
                                 },
                                 sensor_id=PMS5003_SENSOR_ID,
                                 sensor_location=PMS5003_LOCATION,
                                 unit="standard_concentration")
                
                post_measurement(current_timestamp=current_timestamp,
                                 measurement="PM_2.5_std",
                                 value={
                                     "avg": self.avg_pm025_std,
                                     "min": self.min_pm025_std,
                                     "max": self.max_pm025_std
                                 },
                                 sensor_id=PMS5003_SENSOR_ID,
                                 sensor_location=PMS5003_LOCATION,
                                 unit="standard_concentration")
                
                post_measurement(current_timestamp=current_timestamp,
                                 measurement="PM_10_std",
                                 value={
                                     "avg": self.avg_pm100_std,
                                     "min": self.min_pm100_std,
                                     "max": self.max_pm100_std
                                 },
                                 sensor_id=PMS5003_SENSOR_ID,
                                 sensor_location=PMS5003_LOCATION,
                                 unit="standard_concentration")

                ###

                post_measurement(current_timestamp=current_timestamp,
                                 measurement="PM_1_env",
                                 value={
                                     "avg": self.avg_pm010_env,
                                     "min": self.min_pm010_env,
                                     "max": self.max_pm010_env
                                 },
                                 sensor_id=PMS5003_SENSOR_ID,
                                 sensor_location=PMS5003_LOCATION,
                                 unit="environmental_concentration")
                
                post_measurement(current_timestamp=current_timestamp,
                                 measurement="PM_2.5_env",
                                 value={
                                     "avg": self.avg_pm025_env,
                                     "min": self.min_pm025_env,
                                     "max": self.max_pm025_env
                                 },
                                 sensor_id=PMS5003_SENSOR_ID,
                                 sensor_location=PMS5003_LOCATION,
                                 unit="environmental_concentration")
                
                post_measurement(current_timestamp=current_timestamp,
                                 measurement="PM_10_env",
                                 value={
                                     "avg": self.avg_pm100_env,
                                     "min": self.min_pm100_env,
                                     "max": self.max_pm100_env
                                 },
                                 sensor_id=PMS5003_SENSOR_ID,
                                 sensor_location=PMS5003_LOCATION,
                                 unit="environmental_concentration")

                ###

                post_measurement(current_timestamp=current_timestamp,
                                 measurement="particles_gt_0.3um",
                                 value={
                                     "avg": self.avg_particles_003um,
                                     "min": self.min_particles_003um,
                                     "max": self.max_particles_003um
                                 },
                                 sensor_id=PMS5003_SENSOR_ID,
                                 sensor_location=PMS5003_LOCATION,
                                 unit="concentration_per_dL_of_air")

                post_measurement(current_timestamp=current_timestamp,
                                 measurement="particles_gt_0.5um",
                                 value={
                                     "avg": self.avg_particles_005um,
                                     "min": self.min_particles_005um,
                                     "max": self.max_particles_005um
                                 },
                                 sensor_id=PMS5003_SENSOR_ID,
                                 sensor_location=PMS5003_LOCATION,
                                 unit="concentration_per_dL_of_air")

                post_measurement(current_timestamp=current_timestamp,
                                 measurement="particles_gt_1um",
                                 value={
                                     "avg": self.avg_particles_010um,
                                     "min": self.min_particles_010um,
                                     "max": self.max_particles_010um
                                 },
                                 sensor_id=PMS5003_SENSOR_ID,
                                 sensor_location=PMS5003_LOCATION,
                                 unit="concentration_per_dL_of_air")

                post_measurement(current_timestamp=current_timestamp,
                                 measurement="particles_gt_2.5um",
                                 value={
                                     "avg": self.avg_particles_025um,
                                     "min": self.min_particles_025um,
                                     "max": self.max_particles_025um
                                 },
                                 sensor_id=PMS5003_SENSOR_ID,
                                 sensor_location=PMS5003_LOCATION,
                                 unit="concentration_per_dL_of_air")

                post_measurement(current_timestamp=current_timestamp,
                                 measurement="particles_gt_5um",
                                 value={
                                     "avg": self.avg_particles_050um,
                                     "min": self.min_particles_050um,
                                     "max": self.max_particles_050um
                                 },
                                 sensor_id=PMS5003_SENSOR_ID,
                                 sensor_location=PMS5003_LOCATION,
                                 unit="concentration_per_dL_of_air")

                post_measurement(current_timestamp=current_timestamp,
                                 measurement="particles_gt_10um",
                                 value={
                                     "avg": self.avg_particles_100um,
                                     "min": self.min_particles_100um,
                                     "max": self.max_particles_100um
                                 },
                                 sensor_id=PMS5003_SENSOR_ID,
                                 sensor_location=PMS5003_LOCATION,
                                 unit="concentration_per_dL_of_air")

                ###

                read_and_post_dht22_sensors()               # POST TEMPERATURE AND HUMIDITY (DHT22)
                read_and_post_ldr_sensor()                  # POST LIGHT LEVEL (LDR)
                read_and_post_esp32_internal_sensors()      # ESP32 INTERNAL SENSORS
                read_and_post_hcsr501_pir_sensors()

                self.reset()
            else:
                print(".", end="")

                pm010_std = self.pms_obj.pm10_standard
                pm025_std = self.pms_obj.pm25_standard
                pm100_std = self.pms_obj.pm100_standard
                pm010_env = self.pms_obj.pm10_env
                pm025_env = self.pms_obj.pm25_env
                pm100_env = self.pms_obj.pm100_env
                particles_003um = self.pms_obj.particles_03um
                particles_005um = self.pms_obj.particles_05um
                particles_010um = self.pms_obj.particles_10um
                particles_025um = self.pms_obj.particles_25um
                particles_050um = self.pms_obj.particles_50um
                particles_100um = self.pms_obj.particles_100um

                self.avg_pm010_std += pm010_std  / self.num_measures_to_aggregate
                self.avg_pm025_std += pm025_std  / self.num_measures_to_aggregate
                self.avg_pm100_std += pm100_std  / self.num_measures_to_aggregate
                self.avg_pm010_env += pm010_env  / self.num_measures_to_aggregate 
                self.avg_pm025_env += pm025_env  / self.num_measures_to_aggregate 
                self.avg_pm100_env += pm100_env  / self.num_measures_to_aggregate
                self.avg_particles_003um += particles_003um / self.num_measures_to_aggregate
                self.avg_particles_005um += particles_005um / self.num_measures_to_aggregate
                self.avg_particles_010um += particles_010um / self.num_measures_to_aggregate
                self.avg_particles_025um += particles_025um / self.num_measures_to_aggregate
                self.avg_particles_050um += particles_050um / self.num_measures_to_aggregate
                self.avg_particles_100um += particles_100um / self.num_measures_to_aggregate

                self.min_pm010_std = min(self.min_pm010_std, pm010_std)
                self.min_pm025_std = min(self.min_pm025_std, pm025_std)
                self.min_pm100_std = min(self.min_pm100_std, pm100_std)
                self.min_pm010_env = min(self.min_pm010_env, pm010_env)
                self.min_pm025_env = min(self.min_pm025_env, pm025_env)
                self.min_pm100_env = min(self.min_pm100_env, pm100_env)
                self.min_particles_003um = min(self.min_particles_003um, particles_003um)
                self.min_particles_005um = min(self.min_particles_005um, particles_005um)
                self.min_particles_010um = min(self.min_particles_010um, particles_010um)
                self.min_particles_025um = min(self.min_particles_025um, particles_025um)
                self.min_particles_050um = min(self.min_particles_050um, particles_050um)
                self.min_particles_100um = min(self.min_particles_100um, particles_100um)

                self.max_pm010_std = max(self.max_pm010_std, pm010_std)
                self.max_pm025_std = max(self.max_pm025_std, pm025_std)
                self.max_pm100_std = max(self.max_pm100_std, pm100_std)
                self.max_pm010_env = max(self.max_pm010_env, pm010_env)
                self.max_pm025_env = max(self.max_pm025_env, pm025_env)
                self.max_pm100_env = max(self.max_pm100_env, pm100_env)
                self.max_particles_003um = max(self.max_particles_003um, particles_003um)
                self.max_particles_005um = max(self.max_particles_005um, particles_005um)
                self.max_particles_010um = max(self.max_particles_010um, particles_010um)
                self.max_particles_025um = max(self.max_particles_025um, particles_025um)
                self.max_particles_050um = max(self.max_particles_050um, particles_050um)
                self.max_particles_100um = max(self.max_particles_100um, particles_100um)

                self.measure_index += 1

            disconnect_from_wifi()
        except Exception as e:
            msg_str = get_current_datetime_str() + ": " + str(e) + "; An unknown error occurred in PMSCallback.__call__()\n\n"
            write_log(msg_str, print_msg=True)
            if STOP_ON_EXCEPTION:
                raise



# MAIN LOOP ###################################################################

def main():

    try:
        init()
    except Exception as e:
        msg_str = get_current_datetime_str() + ": " + str(e) + "; An unknown error occurred while executing init() in main()\n\n"
        write_log(msg_str, print_msg=True)
        raise

    while True: # <<<
        try:
            connect_to_wifi()
            synchronize_ntp()

            read_and_post_dht22_sensors()                # POST TEMPERATURE AND HUMIDITY (DHT22)
            read_and_post_ldr_sensor()                   # POST LIGHT LEVEL (LDR)
            read_and_post_esp32_internal_sensors()       # ESP32 INTERNAL SENSORS
            read_and_post_hcsr501_pir_sensors()

            disconnect_from_wifi()

        except Exception as e:
            msg_str = get_current_datetime_str() + ": " + str(e) + "; An unknown error occurred while reading sensor values in main()\n\n"
            write_log(msg_str, print_msg=True)
            if STOP_ON_EXCEPTION:
                raise

        # WAIT
        time.sleep(SLEEP_TIME_SEC)


def main_pm5003():
    try:
        init()
    except Exception as e:
        msg_str = get_current_datetime_str() + ": " + str(e) + "; An unknown error occurred while executing init() in main_pm5003()\n\n"
        write_log(msg_str, print_msg=True)
        raise

    loop = asyncio.get_event_loop()
    loop.run_forever()


if __name__ == '__main__':
    if PMS5003_SENSOR_ID is not None:
        main_pm5003()
    else:
        main()
