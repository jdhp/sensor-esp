import time
import ntptime

def synchronize_ntp():
    # C.f. https://docs.micropython.org/en/latest/esp8266/quickref.html#real-time-clock-rtc
    #      https://github.com/micropython/micropython/blob/0fff2e03fe07471997a6df6f92c6960cfd225dc0/ports/esp8266/modules/ntptime.py
    #
    # The NTP host can be configured at runtime by doing: ntptime.host = 'myhost.org'
    # The default NTP host is "pool.ntp.org"
    ntptime.settime() # set the rtc datetime from the remote server


def get_current_timestamp():
    return time.time() + 946684800        # https://forum.micropython.org/viewtopic.php?t=5921
                                          # https://docs.micropython.org/en/latest/library/utime.html#utime.time

def get_current_datetime_str(timestamp=None):    
    time_tuple = time.localtime(timestamp)
    return "{}-{:02d}-{:02d}T{:02d}:{:02d}:{:02d}+0000".format(*time_tuple)