import ubinascii
import machine

# C.f. https://docs.micropython.org/en/latest/library/machine.html#machine.unique_id and https://forum.micropython.org/viewtopic.php?t=2770

def get_unique_id():
    esp_unique_id = ubinascii.hexlify(machine.unique_id()).decode('utf-8')
    return esp_unique_id

if __name__ == '__main__':
    print(get_unique_id())