from machine import Pin
import time

def hcsr501_pir_callback(pin):
    print(time.time(), pin.value())

pir_pin = Pin(14, Pin.IN)

#pir_pin.irq(trigger=Pin.IRQ_RISING, handler=hcsr501_pir_callback)
pir_pin.irq(trigger=Pin.IRQ_RISING | Pin.IRQ_FALLING, handler=hcsr501_pir_callback)
