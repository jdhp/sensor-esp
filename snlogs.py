from configsystem import WRITE_LOGS_ON_FLASH

def write_log(msg, filename='errors.log', print_msg=True):
    if print_msg:
        print(msg)

    if WRITE_LOGS_ON_FLASH:
        f = open(filename, 'a')
        f.write(msg)
        f.close()